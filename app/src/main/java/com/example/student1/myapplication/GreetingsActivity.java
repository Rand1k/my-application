package com.example.student1.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class GreetingsActivity extends AppCompatActivity {
    TextView tGreetings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greetings);
        tGreetings = (TextView) findViewById(R.id.tr);

        Intent intent = getIntent();
        String username = intent.getExtras().getString("login");

        tGreetings.setText("Hello, "+ username);
    }
}
