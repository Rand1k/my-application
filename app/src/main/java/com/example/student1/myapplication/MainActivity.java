package com.example.student1.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener  {
    EditText login;
    EditText password;
    EditText repassword;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.but);
        button.setOnClickListener(this);
        login = (EditText) findViewById(R.id.etl);
        password = (EditText) findViewById(R.id.etp);
        repassword = (EditText) findViewById(R.id.etr);

    }

    public void onClick(View view) {
        String username = login.getText().toString();

        Intent intent = new Intent(this,GreetingsActivity.class);
        intent.putExtra("login",username);

        startActivity(intent);
    }
}
